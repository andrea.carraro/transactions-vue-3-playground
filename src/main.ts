import { createApp } from 'vue-demi';
import { App } from './index';

createApp(App).mount('#app');
