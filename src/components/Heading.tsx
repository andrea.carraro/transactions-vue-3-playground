import { defineComponent, PropType } from 'vue-demi';

export default defineComponent({
  name: 'Heading',
  props: {
    text: {
      type: [String] as PropType<string>,
      required: true,
    },
  },
  setup(props) {
    return () => <h1>{props.text}</h1>;
  },
});
