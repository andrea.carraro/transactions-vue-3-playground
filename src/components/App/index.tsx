import { defineComponent } from 'vue-demi';
// import Counter from '../Counter.vue';
import CounterTsx from '../CounterTsx';
import style from './style.module.css';
import './style.global.css';
import logo from './logo.png';

export default defineComponent({
  name: 'App',
  setup() {
    return () => (
      <div class={style.app}>
        <img alt="Vue logo" src={logo} />
        {/* <Counter msg="Counter" /> */}
        <CounterTsx msg="CounterTsx" />
      </div>
    );
  },
});
