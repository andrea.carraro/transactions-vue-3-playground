import { defineComponent, PropType, computed, ref, reactive } from 'vue-demi';
import Heading from '../Heading';
import style from './style.module.css';

export default defineComponent({
  name: 'CounterTsx',
  props: {
    msg: {
      type: [String] as PropType<string>,
      required: true,
    },
  },
  setup(props) {
    const count = ref<number>(0);
    return () => (
      <div class={style.counter}>
        <button
          onClick={() => {
            count.value--;
          }}
        >
          -
        </button>
        <button
          onClick={() => {
            count.value++;
          }}
        >
          +
        </button>
        <div>{count.value}</div>
        <Heading text={'Msg: ' + props.msg} />
      </div>
    );
  },
});
