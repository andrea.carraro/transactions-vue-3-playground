import alias from '@rollup/plugin-alias';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import path from 'path';
import { babel } from '@rollup/plugin-babel';
import typescript from 'rollup-plugin-typescript2';
import ttypescript from 'ttypescript';
import image from '@rollup/plugin-image';
import postcss from 'rollup-plugin-postcss';
import pkg from './package.json';

const SRC_PATH = path.resolve(__dirname, 'src');
const customResolver = nodeResolve({
  extensions: ['.js', '.json', '.ts', '.tsx', '.vue', '.css', '.scss'],
});
// Mark dependencies and peerDependencies as external
const external = [
  ...Object.keys(pkg.dependencies || {}),
  ...Object.keys(pkg.peerDependencies || {}),
];

/**
 * Returns A rollup config block
 *
 * @param {('cjs'|'es')} format
 * @returns rollup config for a specific target
 */
async function makeConfig({ format = 'cjs', generateCSS = true, generateTSDeclarations = true }) {
  return {
    input: 'src/index.ts',
    external,
    plugins: [
      customResolver,
      commonjs(),
      alias({
        entries: [{ find: /^@/, replacement: SRC_PATH }],
        customResolver,
      }),
      typescript({
        tsconfig: 'tsconfig.build.json',
        useTsconfigDeclarationDir: true,
        typescript: ttypescript,
        clean: true,
        tsconfigOverride: {
          declaration: generateTSDeclarations,
        },
      }),
      babel({ babelHelpers: 'bundled', extensions: ['.js', '.jsx', '.ts', '.tsx'] }),
      image(),
      postcss({
        extract: path.resolve(pkg.style),
      }),
    ],
    output: {
      file: format === 'cjs' ? pkg.main : pkg.module,
      format,
      sourcemap: true,
    },
  };
}

export default Promise.all([
  makeConfig({ format: 'es', generateCSS: true, generateTSDeclarations: true }),
  makeConfig({ format: 'cjs', generateCSS: false, generateTSDeclarations: false }),
]);
